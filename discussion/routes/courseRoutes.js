// Dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");

// for importing courseController
const courseController = require("../controllers/courseController")

// Router for creating course

router.post("/addCourse", auth.verify, (req,res) => {
    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    courseController.addCourse(data).then(result => res.send(result));
});

// Activity
// Function that retrieving all the courses
router.get("/courseList", (req, res) => {

	courseController.getAllCourse().then(
		resultFromController => res.send(resultFromController));
});

//  Route for updating the course

router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});

router.get("/getAllActive", (req, res) => {
    courseController.getAllActive().then(
        result => res.send(result
            )
    );
});
//  Routes for archiving
//  Using Params
router.put("/archive/courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(result => res.send(result));
});

/*
	Plan for Capstone:

	Admin - Manage - (Product - All or limited - Active or Inactive) Ans: Both

	Users - Bumili - (Product - All or Limited - Active or Inactive) Ans: 

*/

module.exports = router